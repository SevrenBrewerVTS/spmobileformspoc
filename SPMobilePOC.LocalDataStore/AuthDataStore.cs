﻿using System;
using System.IO;
using System.Threading.Tasks;
using SPMobilePOC.LocalDataStore.StorageModels;
using SQLite;

namespace LocalDataStore
{
    public class AuthDataStore
    {
        private static AuthDataStore _instance;
        public static AuthDataStore Instance => _instance ?? (_instance = new AuthDataStore());

        private SQLiteAsyncConnection _connection;

        private string _databasePath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AppStorage.db");
            }
        }

        public AuthDataStore()
        {
            Task.Run(async () =>
            {
                try
                {
                    _connection = new SQLiteAsyncConnection(_databasePath);
                    await _connection.CreateTableAsync<UserNameStorageModel>();

                }
                catch (Exception e)
                {
                    Console.WriteLine($"There was an error {e}");
                }
            });
        }

        public async Task<int> SetCurrentUser(UserNameStorageModel user)
        {
            //if (_connection == null) _connection = new SQLiteAsyncConnection(_databasePath);

            await _connection.ExecuteAsync(@"DELETE FROM UserNameStorageModel");
            return await _connection.InsertAsync(user);
        }

        public async Task<string> GetCurrentAuthToken()
        {
            //if (_connection == null) _connection = new SQLiteAsyncConnection(_databasePath);
            var item = await _connection.Table<UserNameStorageModel>().FirstOrDefaultAsync();

            if (item != null)
            {
                return item.AuthToken;
            }
            else
            {
                return null;
            }
        }
    }
}
