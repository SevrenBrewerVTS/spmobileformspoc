﻿
//using System;
//using System.IO;
//using System.Threading.Tasks;
//using SQLite;

//namespace LocalDataStore
//{


//    public static class LocalAppStore<TEntity> where TEntity : new()
//    {
//        private readonly SQLiteAsyncConnection _sqliteConnectionAsync;

//        protected SQLiteAsyncConnection ConnectionAsync { get { return _sqliteConnectionAsync; } }

//        //protected BaseRepo( /* IMvxSqliteConnectionFactory sqliteConnectionFactory  */)
//        //{
//        //try
//        //{
//        //    _sqliteConnectionAsync = sqliteConnectionFactory.GetAsyncConnection(Services.DataService.DatabaseName);
//        //    _sqliteConnectionAsync.CreateTableAsync<TEntity>();
//        //}
//        //catch (Exception ex)
//        //{
//        //    MvvmCross.Platform.Mvx.Error(ex.ToString());
//        //}
//        //}



//        private string _databasePath
//        {
//            get
//            {
//                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AppStorage.db");
//            }
//        }

//        private SQLiteAsyncConnection _connection;

//        public LocalAppStore()
//        {
//            Task.Run(async () =>
//            {
//                try
//                {
//                    _connection = new SQLiteAsyncConnection(_databasePath);
//                    await _connection.CreateTableAsync<TEntity>();

//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine($"There was an error {e}");
//                }
//            });
//        }



//        protected async Task RecreateTableAsync()
//        {
//            await _connection.DropTableAsync<TEntity>();
//            await _connection.CreateTableAsync<TEntity>();
//        }

//    }
//}
