﻿using System;
namespace SPMobilePOC.LocalDataStore.StorageModels
{
    public class UserNameStorageModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string AuthToken { get; set; }
    }
}
