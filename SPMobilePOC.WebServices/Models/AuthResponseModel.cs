﻿
using Newtonsoft.Json;

namespace WebService.Models
{
    public class AuthResponseModel
    {

        public enum ResponceCode
        {
            Success,
            Fail
        }

        [JsonIgnore]
        public ResponceCode ResponseCode { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
}
