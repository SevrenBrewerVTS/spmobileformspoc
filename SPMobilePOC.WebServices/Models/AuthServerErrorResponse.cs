﻿
using Newtonsoft.Json;

namespace WebService.Models
{
    public class AuthServerErrorResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }
}
