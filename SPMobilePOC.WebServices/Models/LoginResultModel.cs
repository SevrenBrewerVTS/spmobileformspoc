﻿using System;
namespace WebService.Models
{

    public enum LoginResultStatus
    {
        Fail,
        Success
    }

    public class LoginResultModel
    {
        public LoginResultStatus Status
        { get; }
        public string Message { get; set; }

        public LoginResultModel(LoginResultStatus status, string message = null)
        {
            Status = status;
            Message = message;
        }
    }

}
