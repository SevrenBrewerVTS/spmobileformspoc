﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebService.Models
{
    public class UserModel
    {
        [JsonProperty("unique_name")]
        public string UserName { get; set; }

        //[JsonIgnore]
        [JsonProperty("role")]
        //[JsonConverter(typeof(SingleOrArrayConverter<string>))]
        public List<string> Roles { get; set; }

        //private string _rolesString;
        public string RolesString { get; set; }
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_rolesString) && Roles != null && Roles.Count > 0)
        //            _rolesString = string.Join(";", Roles);

        //        return _rolesString;
        //    }
        //    set
        //    {
        //        if (SetProperty(ref _rolesString, value))
        //        {
        //            Roles = _rolesString.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        //        }
        //    }
        //}

        // investigate this further


        //[JsonIgnore]
        //[JsonProperty("role")]
        //[JsonConverter(typeof(SingleOrArrayConverter<string>))]
        //public List<string> Roles { get; set; }

        //private string _rolesString;
        //public string RolesString //{ get; set; }
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_rolesString) && Roles != null && Roles.Count > 0)
        //            _rolesString = string.Join(";", Roles);

        //        return _rolesString;
        //    }
        //    set
        //    {
        //        if (SetProperty(ref _rolesString, value))
        //        {
        //            Roles = _rolesString.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        //        }
        //    }
        //}

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("vts.prs")]
        public bool RememberMe { get; set; }

        [JsonProperty("vts.uid")]
        public Guid UserId { get; set; }

        [JsonProperty("vts.csst")]
        public DateTime CurrentSessionStartTime { get; set; }

        [JsonProperty("vts.rfs")]
        public bool RequiredFieldsSupplied { get; set; }

        [JsonProperty("vts.acrd")]
        public DateTime AccountCreatedDate { get; set; }

        [JsonProperty("vts.sid")]
        public Guid SessionId { get; set; }

        [JsonProperty("vts.fn")]
        public string FirstName { get; set; }

        [JsonProperty("vts.ln")]
        public string LastName { get; set; }

        [JsonProperty("vts.ttl")]
        public string Title { get; set; }

        [JsonProperty("vts.lpwct")]
        public DateTime LastPasswordChangeTime { get; set; }

        [JsonProperty("vts.ouid")]
        public int OfficeUserId { get; set; }

        [JsonProperty("vts.outyp")]
        public int OfficeUserType { get; set; }

        [JsonProperty("vts.mlm")]
        public bool ManageLandmarks { get; set; }

        [JsonProperty("vts.hrpt")]
        public bool HasReporting { get; set; }

        [JsonProperty("vts.hal")]
        public bool HasAlerts { get; set; }

        [JsonProperty("vts.hgf")]
        public bool HasGeofencing { get; set; }

        [JsonProperty("vts.hm")]
        public bool HasMaintenance { get; set; }

        [JsonProperty("vts.hsg")]
        public bool HasSpeedGauge { get; set; }

        [JsonProperty("vts.hg")]
        public bool HasGarmin { get; set; }

        [JsonProperty("vts.ma")]
        public bool HasMobileAccess { get; set; }

        [JsonProperty("vts.mgid")]
        public int MobileGroupId { get; set; }

        [JsonProperty("vts.mgn")]
        public string MobileGroupName { get; set; }

        [JsonProperty("vts.spt")]
        public int SpeedThreshold { get; set; }

        [JsonProperty("vts.utcos")]
        public string UtcOffset { get; set; }

        [JsonProperty("vts.drid")]
        public int DriverId { get; set; }

        [JsonProperty("vts.drn")]
        public string DriverName { get; set; }

        [JsonProperty("iss")]
        public string iss { get; set; }

        [JsonProperty("aud")]
        public string aud { get; set; }

        [JsonProperty("exp")]
        public int exp { get; set; }

        [JsonProperty("nbf")]
        public int nbf { get; set; }

        public string AccessToken { get; private set; }
        public string RefreshToken { get; private set; }

        [JsonIgnore]
        public DateTimeOffset SessionExpirationDateTime
        {
            get
            {
                if (exp > 0)
                {
                    return new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.FromMilliseconds(0)).Add(TimeSpan.FromSeconds(exp));
                }

                return DateTimeOffset.Now;
            }
        }

        public static UserModel FromJwt(string accessToken, string refreshToken = null)
        {
            var user = JWT.JsonWebToken.DecodeToObject<UserModel>(accessToken, Base64UrlDecode("lPZfojt_xZWhEih8ynHNDfhGB0gqlE52TINhqf8uxyU"));
            //user.AccessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1laWQiOiIxMDRmZTgxYi1iODFmLTRkM2QtYTcxOS00MGMyZTIxM2Q2NzYiLCJ1bmlxdWVfbmFtZSI6ImRkb2thZHplIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS9hY2Nlc3Njb250cm9sc2VydmljZS8yMDEwLzA3L2NsYWltcy9pZGVudGl0eXByb3ZpZGVyIjoiQVNQLk5FVCBJZGVudGl0eSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiOTZhMDJlZTUtYmQzYS00ZDIyLWFiODktY2Q3MWI4YTQ1ZGVmIiwicm9sZSI6WyJHYXJtaW5Vc2VyIiwiVlRTUGVyc29uZWwiLCJMYW5kbWFya0NyZWF0b3IiLCJWVFNTYWxlc01hbmFnZXIiLCJTdWJHcm91cEFkbWluIiwiTW9iaWxlR3JvdXBBZG1pbiIsIlN1cGVyQWRtaW4iLCJHX1BlcnNvbmVsIiwiVlRTQmlsbGluZyIsIkRyaXZlciIsIkdfQWRtaW4iLCJTbmFwc2hvdE1hbmFnZXIiLCJHZW9mZW5jZUVkaXRvciIsIlZUU1Rlc3RlciIsIkdlb2ZlbmNlQ3JlYXRvciIsIkxhbmRtYXJrRWRpdG9yIiwiVlRTQWRtaW4iLCJNb2JpbGVHcm91cFVzZXIiXSwiZW1haWwiOiJkaW1pdHJpQHZlaGljbGV0cmFja2luZy5jb20iLCJ2dHMucHJzIjoiRmFsc2UiLCJ2dHMudWlkIjoiMTA0ZmU4MWItYjgxZi00ZDNkLWE3MTktNDBjMmUyMTNkNjc2IiwidnRzLmNzc3QiOiI2LzI5LzIwMTcgMTE6MDk6NDAgUE0iLCJ2dHMucmZzIjoiVHJ1ZSIsInZ0cy5hY3JkIjoiMy8yNS8yMDEwIDg6NTQ6NDUgUE0iLCJ2dHMuc2lkIjoiM2Y5YzZiMWYtZTRlMy00MGUxLWExZTItZmZjNjY4ZGQzOGQyIiwidnRzLmZuIjoiRGltaXRyaSIsInZ0cy5sbiI6IkRva2FkemUiLCJ2dHMudHRsIjoiU3IuIFNvZnR3YXJlIERldmVsb3BlciIsInZ0cy5scHdjdCI6IjUvMTMvMjAxNiAxOjA3OjIzIFBNIiwidnRzLm91aWQiOiIzNDYxIiwidnRzLm91dHlwIjoiMSIsInZ0cy5tbG0iOiJUcnVlIiwidnRzLmhycHQiOiJUcnVlIiwidnRzLmhhbCI6IlRydWUiLCJ2dHMuaGdmIjoiVHJ1ZSIsInZ0cy5obSI6IlRydWUiLCJ2dHMuaHNnIjoiVHJ1ZSIsInZ0cy5oZyI6IlRydWUiLCJ2dHMubWEiOiJUcnVlIiwidnRzLm1naWQiOiIxIiwidnRzLm1nbiI6IlZUUyBDb3Jwb3JhdGUiLCJ2dHMuc3B0IjoiNjUiLCJ2dHMudXRjb3MiOiItNHxFRFQiLCJ2dHMuZHJpZCI6IjYzMTciLCJ2dHMuZHJuIjoiRGltaXRyaXVzIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLnZlaGljbGV0cmFja2luZy5jb20iLCJhdWQiOiI4MjQ3M2NhMGM4OWI0YjU3YWFlZGMxNzVmNWE4NTMyNSIsImV4cCI6MTQ5ODkzODkzMiwibmJmIjoxNDk4ODUyNTMyfQ.8vrpLr-1kJCQYbGgpPwP0GafGoZ21e5gx_vkkkDsVoI";
            user.AccessToken = accessToken;
            user.RefreshToken = refreshToken;
            return user;
        }

        private static byte[] Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+'); // 62nd char of encoding
            output = output.Replace('_', '/'); // 63rd char of encoding
            switch (output.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: output += "=="; break; // Two pad chars
                case 3: output += "="; break; // One pad char
                default: throw new Exception("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output); // Standard base64 decoder
            return converted;
        }
    }

    //class SingleOrArrayConverter<T> : JsonConverter
    //{
    //    public override bool CanConvert(Type objectType)
    //    {
    //        return (objectType == typeof(List<T>));
    //    }

    //    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    //    {
    //        JToken token = JToken.Load(reader);
    //        if (token.Type == JTokenType.Array)
    //        {
    //            return token.ToObject<List<T>>();
    //        }
    //        return new List<T> { token.ToObject<T>() };
    //    }

    //    public override bool CanWrite
    //    {
    //        get { return false; }
    //    }

    //    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
