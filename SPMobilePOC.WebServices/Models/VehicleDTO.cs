﻿using System;
using System.Collections.Generic;

namespace SPMobilePOC.WebServices.Models
{
    public class VehicleDTO
    {
        public int VehicleID { get; set; }
        public string VehicleName { get; set; }
        public long VehicleLocationID { get; set; }
        public int LandmarkID { get; set; }
        public int LandmarkCategoryID { get; set; }
        public VehicleStatus VehicleStatus { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string DriverName { get; set; }
        public int DriverID { get; set; }
        public string Notes { get; set; }
        public double Speed { get; set; }
        public string Heading { get; set; }
        public double HeadingDegrees { get; set; }
        public string ReadingTime { get; set; }
        public string LastUpdate { get; set; }
        public DateTime ReadingTimeUTC { get; set; }
        public string StartOfStopTime { get; set; }
        public string StopTime { get; set; }
        public bool EngineOn { get; set; }
        public bool PTOAvailable { get; set; }
        public bool SpeedGaugeEnable { get; set; }
        public bool IsSpeeding { get; set; }
        public int SpeedLimit { get; set; }
        public int DTCVersion { get; set; }
        public string IdleTime { get; set; }
        public bool IsPortable { get; set; }
        public int BatteryLevel { get; set; }
        public bool HasGarmin { get; set; }
        public bool HasFuelCard { get; set; }
        public bool HasDriverId { get; set; }
        public bool HasRouting { get; set; }
        public string IMEI { get; set; }
        public int SubgroupID { get; set; }
        public int MobileGroupID { get; set; }
        public List<TicketSummary> TicketSummaries { get; set; }
        public List<RouteSummary> RouteSummary { get; set; }
        public List<VehicleIO> IO { get; set; }
        public bool HasLocateNow { get; set; }
        public string VehicleMakeModel { get; set; }
        public string PlateNumber { get; set; }
        public int? UserID { get; set; }
        public string SubgroupName { get; set; }
        public bool ShowDispatchTab { get; set; }
        public string TemperatureReadingTime { get; set; }
        public decimal? TemperatureReadingOne { get; set; }
        public decimal? TemperatureReadingTwo { get; set; }
        public bool NonResponsive { get; set; }
        public bool PowerOff { get; set; }
        public CustomVehicleStatus CustomVehicleStatus { get; set; }
        public string CameraIdentifier { get; set; }
        public int CameraManufacturerId { get; set; }
        public bool HasDiagnostics { get; set; }




    }

    public class VehicleStatus
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public int IconWidth { get; set; }
        public int IconHeight { get; set; }
        public byte[] IconBytes { get; set; }
    }

    public class TicketSummary
    {
        public string Summary { get; set; }
        public SuperAddress Address { get; set; }
    }

    public class SuperAddress
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Street { get; set; }
        public string Apartment { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FormattedAddress { get { return this.ToFormattedString(", "); } private set { this.FormattedAddress = value; } }

        public string ToFormattedString(string Separator)
        {
            string retVal = string.Empty;
            List<String> AddressList = new List<string>();

            if (!string.IsNullOrEmpty(Name))
                AddressList.Add(Name);

            if (!string.IsNullOrEmpty(Phone))
                AddressList.Add(string.Format("T: {0}", Phone));

            if (!string.IsNullOrEmpty(Cell))
                AddressList.Add(string.Format("C: {0}", Cell));

            if (!string.IsNullOrEmpty(Street))
                AddressList.Add(Street);

            if (!string.IsNullOrEmpty(Apartment))
                AddressList.Add(Apartment);

            string tempString = string.Empty;
            if (!string.IsNullOrEmpty(City))
                tempString = City;

            if (!string.IsNullOrEmpty(State))
                tempString = !string.IsNullOrEmpty(tempString) ? string.Format("{0}, {1}", tempString, State) : State;

            if (!string.IsNullOrEmpty(Zip))
                tempString = !string.IsNullOrEmpty(tempString) ? string.Format("{0} {1}", tempString, Zip) : Zip;

            if (!string.IsNullOrEmpty(tempString))
                AddressList.Add(tempString);

            retVal = string.Join(Separator, AddressList.ToArray());

            return retVal;
        }
    }

     public class RouteSummary
    {
        public string RouteName { get; set; }
        public string RouteOriginName { get; set; }
        public bool RouteCompleted { get; set; }
        public int StopPosition { get; set; }
        public string StopName { get; set; }
        public bool StopSent { get; set; }
        public bool StopActive { get; set; }
        public bool StopCompleted { get; set; }
        public bool StopDeleted { get; set; }
        public SuperAddress Address { get; set; }
        public string StopNotes { get; set; }
    }

     public class VehicleIO
    {
        public int? ID { get; set; }
        public int VehicleID { get; set; }
        public IOType IO { get; set; }
        public string Name { get; set; }
        public bool Invert { get; set; }
        public bool IsEngaged { get; set; }
        public bool TwoWay { get; set; }
    }

    public enum IOType
    {
        None = 0,
        IO1 = 1,
        IO2 = 2,
        IO3 = 3,
        IO4 = 4,
        IO5 = 5
    }

     public class CustomVehicleStatus
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public int DefinitionId { get; set; }
        public int BinaryFileId { get; set; }
        public DateTime ReadingTime { get; set; }
        public DateTime? StatusExpires { get; set; }
        public string Notes { get; set; }
        public bool Active { get; set; }
    }
}
