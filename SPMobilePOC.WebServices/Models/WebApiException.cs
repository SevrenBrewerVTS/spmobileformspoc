﻿
using System;
using System.Net;

namespace WebService.Models
{
    public class WebApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public WebApiException(string message) : base(message) { }

        public WebApiException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
