﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SPMobilePOC.WebServices.Models;
using WebService.Models;

namespace WebService
{
    public class WebApiService
    {
        private static WebApiService _instance;
        public static WebApiService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WebApiService();
                }

                return _instance;
            }
        }

        private const string _baseApiAddress = "https://api.silentpassenger.com/rest/v3/";
        private const string _baseAuthAddress = "https://auth.vehicletracking.com/oauth2/token";

        public class AuthRequest : HttpRequestMessage
        {
            private const string _clientId = "82473ca0c89b4b57aaedc175f5a85325";

            public AuthRequest(string url, string username, string password)
            {
                RequestUri = new Uri(url);
                Method = HttpMethod.Post;
                Content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("client_id", _clientId)
            });
            }

            public AuthRequest(string url, string refreshToken)
            {
                RequestUri = new Uri(url);
                Method = HttpMethod.Post;
                Content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("refresh_token", refreshToken),
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
                new KeyValuePair<string, string>("client_id", _clientId)
            });
            }
        }

        private WebApiException GetException(HttpResponseMessage response)
        {
            string responseBody = response.Content.ReadAsStringAsync().Result;
            return new WebApiException(responseBody, response.StatusCode);
        }

        public async Task<AuthResponseModel> LoginAsync(string username, string password)
        {
            using (var client = new HttpClient())
            {
                using (var request = new AuthRequest(_baseAuthAddress, username, password) { Method = HttpMethod.Post })
                {
                    using (var response = await client.PostAsync(request.RequestUri, request.Content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string resultContent = await response.Content.ReadAsStringAsync();
                            var retVal = JsonConvert.DeserializeObject<AuthResponseModel>(resultContent);
                            retVal.ResponseCode = AuthResponseModel.ResponceCode.Success;
                            return retVal;
                        }

                        return new AuthResponseModel { ResponseCode = AuthResponseModel.ResponceCode.Fail };
                        //return GetException(response);
                    }
                }
            }
        }

        public async Task<IEnumerable<VehicleDTO>> GetVehiclesAsync(string authtoken)
        {
            var url = "vehicle-locations";

            try
            {
                using (var client = new HttpClient { BaseAddress = new Uri(_baseApiAddress) })
                {
                    using (var response = await client.SendAsync(GETRequest(url, authtoken)))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string responseContent = await response.Content.ReadAsStringAsync();
                            try
                            {
                                var retVal = JsonConvert.DeserializeObject<IEnumerable<VehicleDTO>>(responseContent);
                                return retVal;
                            }
                            catch (Exception e)
                            {
                                return null;
                            }
                        }

                        throw GetException(response);
                    }
                }

            }
            catch (WebApiException ex)
            {
                //if (ex.StatusCode == HttpStatusCode.Unauthorized)
                //    _messenger.Publish(new UnauthorizedAccessMessage(this, _authorizedUser));

                throw ex;
            }
        }

        private HttpRequestMessage GETRequest(string uri, string authtoken)
        {
            return SecuredRequestMessage(HttpMethod.Get, uri, authtoken);
        }

        private HttpRequestMessage POSTRequest(string uri, HttpContent content, string authtoken)
        {
            var request = SecuredRequestMessage(HttpMethod.Post, uri, authtoken);
            request.Content = content;

            return request;
        }

        private HttpRequestMessage SecuredRequestMessage(HttpMethod method, string uri, string authToken)
        {
            //CheckAuthStatus();

            //var now = DateTimeOffset.Now;
            //var utcNow = DateTimeOffset.UtcNow;

            //var expires = _authorizedUser.SessionExpirationDateTime;

            //if (utcNow.AddHours(12) > expires && _authorizedUser != null)
            //    _messenger.Publish(new UnauthorizedAccessMessage(this, _authorizedUser));

            //if (_authorizedUser == null) throw new UnauthorizedAccessException("User is not logged in");



            var request = new HttpRequestMessage(method, uri);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authToken);

            return request;
        }

        private bool CheckAuthStatus()
        {
            //var now = DateTimeOffset.Now;
            //var utcNow = DateTimeOffset.UtcNow;

            //var expires = _authorizedUser.SessionExpirationDateTime;

            //if (utcNow.AddHours(12) > expires && _authorizedUser != null)
            //    _messenger.Publish(new UnauthorizedAccessMessage(this, _authorizedUser));

            return true;
        }
    }
}
