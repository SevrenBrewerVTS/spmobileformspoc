﻿using System;
using System.Net.Http;
using WebService.Models;

namespace SPMobilePOC.Extentions
{
    public static class ExtentionMethods
    {
        public static WebApiException ApiExceptionFromResponse(this HttpResponseMessage message)
        {
            string responseBody = message.Content.ReadAsStringAsync().Result;
            return new WebApiException(responseBody, message.StatusCode);
        }
    }
}
