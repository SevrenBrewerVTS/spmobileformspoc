﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SPMobilePOC.WebServices.Models;

namespace WebService
{
    public class FleetManager
    {


        private static FleetManager _instance;
        public static FleetManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FleetManager();
                }

                return _instance;
            }
        }

        public ObservableCollection<VehicleDTO> FleetVehicles { get; set; }

        public async Task GetFleet()
        {
            var authToken = await UserSessionManager.Instance.GetCurrentAuthToken();
            var fleet = await WebApiService.Instance.GetVehiclesAsync(authToken);


            FleetVehicles = new ObservableCollection<VehicleDTO>(fleet);
        }
    }
}


//public sealed class CauseTransactionManager
//{
//public event EventHandler CollectionChanged;

//private static CauseTransactionManager _instance;
//public static CauseTransactionManager Instance
//{
//    get
//    {
//        if (_instance == null)
//        {
//            _instance = new CauseTransactionManager();
//        }

//        return _instance;
//    }
//}

//public enum PaymentType
//{
//    Cash, Check, Card, QR, QRScan, FaceScan, Swiped
//}

//private TransactionGroupModel _transactionGroup;

//public ReadOnlyCollection<CheckoutTransactionModel> Models
//{
//    get
//    {
//        return _transactionGroup.CheckoutTransactions.AsReadOnly();
//    }
//}

//public void AddToTransactionGroup(CheckoutTransactionModel item)
//{
//    item.GroupGuid = _transactionGroup.Guid;
//    _transactionGroup.CheckoutTransactions.Add(item);
//    CollectionChanged?.Invoke(this, EventArgs.Empty);
//}

//public void RemoveFromTransactionGroup(CheckoutTransactionModel item)
//{
//    _transactionGroup.CheckoutTransactions.Remove(item);
//    CollectionChanged?.Invoke(this, EventArgs.Empty);
//}

//public void ClearTransactionGroup()
//{
//    _transactionGroup.CheckoutTransactions.Clear();
//    CollectionChanged?.Invoke(this, EventArgs.Empty);
//}

//public CauseTransactionManager()
//{
//    _transactionGroup = new TransactionGroupModel();
//}

//public string ToJsonString()
//{
//    return _transactionGroup.ToJsonString();
//}

//public async Task<(CauseError errorDetails, CreateBillResponseModel responseModel)> CreateBill(string token, string discountType, string tip,
//string discountAmount, string subtotal, string memo, string discountRate, string tax, string inputAmount, string merchantId)
//{
//    CauseError errDetails = new CauseError { Title = "unknown_error", Code = CauseErrorCode.Fail, Description = "unknown_error" };

//    using (var client = new HttpClient())
//    {
//        client.Timeout = TimeSpan.FromSeconds(30);
//        client.BaseAddress = ApiBase.Uri;
//        client.DefaultRequestHeaders.Accept.Clear();
//        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

//        var bill = new Dictionary<string, object>
//        {
//            { "discountType", discountType },
//            { "tip", tip },
//            { "discountAmount", discountAmount },
//            { "subtotal", subtotal },
//            { "memo", memo },
//            { "discountRate", discountRate },
//            { "tax", tax },
//            { "input_amount", inputAmount },
//            { "merchant_id", merchantId }
//        };

//        var payload = new Dictionary<string, object> {
//            { "token", token},
//            { "transaction", bill}
//        };

//        var stringPayload = JsonConvert.SerializeObject(payload);
//        var postVariables = new StringContent(stringPayload, Encoding.UTF8, "application/json");

//        string response = "";
//        try
//        {
//            HttpResponseMessage responseMessage = await client.PostAsync($"{ApiBase.Uri}api/v2/transactions/billcreate", postVariables).ConfigureAwait(false);
//            response = await responseMessage.Content.ReadAsStringAsync();
//            var result = JsonConvert.DeserializeObject<IDictionary<string, object>>(response);

//            if (result.ContainsKey("error"))
//            {
//                var err = JsonConvert.DeserializeObject<CauseErrorDTO>(response);
//                errDetails = new CauseError { Code = CauseErrorCode.Fail, Title = err.Error.Title, Description = err.Error.Description };
//                return (errDetails, new CreateBillResponseModel());
//            }

//            var responseDto = JsonConvert.DeserializeObject<CreateBillResultDTO>(response, Helpers.GetJsonSettings());
//            var returnModel = DTOConverter.ConvertDTOToDataModel(responseDto);
//            errDetails = new CauseError { Code = CauseErrorCode.Success, Title = "success", Description = "success" };
//            return (errDetails, returnModel);
//        }
//        catch (Exception ex)
//        {
//            Crashes.TrackError(ex);
//            Debug.WriteLine($"Exception: {ex.Message}");
//            return (errDetails, new CreateBillResponseModel());
//        }
//    }
//}

//public async Task<(CauseError errorDetails, PayWithScripResponseModel responseModel)> PayWithScrip(string token, string merchantId,
//string qrCode, string billId, double amount)
//{
//    CauseError errDetails = new CauseError { Title = "unknown_error", Code = CauseErrorCode.Fail, Description = "unknown_error" };

//    using (var client = new HttpClient())
//    {
//        client.Timeout = TimeSpan.FromSeconds(30);
//        client.BaseAddress = ApiBase.Uri;
//        client.DefaultRequestHeaders.Accept.Clear();
//        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

//        var payload = new Dictionary<string, object> {
//                { "paymethod", "scrip"},
//                { "qrcode", qrCode},
//                { "token", token},
//                { "bill_id", billId},
//                { "amount", amount}
//            };

//        var stringPayload = JsonConvert.SerializeObject(payload);
//        var postVariables = new StringContent(stringPayload, Encoding.UTF8, "application/json");

//        string response = "";
//        try
//        {
//            HttpResponseMessage responseMessage = await client.PostAsync($"/api/v2/merchants/{merchantId}/debit_scrip", postVariables).ConfigureAwait(false);
//            response = await responseMessage.Content.ReadAsStringAsync();
//            var result = JsonConvert.DeserializeObject<IDictionary<string, object>>(response);

//            if (result.ContainsKey("error"))
//            {
//                var err = JsonConvert.DeserializeObject<CauseErrorDTO>(response);
//                errDetails = new CauseError { Code = CauseErrorCode.Fail, Title = err.Error.Title, Description = err.Error.Description };
//                return (errDetails, new PayWithScripResponseModel());
//            }

//            var responseDto = JsonConvert.DeserializeObject<PayWithScripResponseDTO>(response, Helpers.GetJsonSettings());
//            var returnModel = DTOConverter.ConvertDTOToDataModel(responseDto);
//            errDetails = new CauseError { Code = CauseErrorCode.Success, Title = "Success", Description = "Success" };
//            return (errDetails, returnModel);

//        }
//        catch (Exception ex)
//        {
//            Crashes.TrackError(ex);
//            Debug.WriteLine($"Exception: {ex.Message}");
//            return (errDetails, new PayWithScripResponseModel());
//        }
//    }
//}
