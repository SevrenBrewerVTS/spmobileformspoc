﻿using System;
using System.Threading.Tasks;
using LocalDataStore;
using Newtonsoft.Json;
using SPMobilePOC.LocalDataStore.StorageModels;
using WebService.Models;

namespace WebService
{

    public class UserSessionManager
    //:

    //MvxViewModel, IAuthenticationService
    {




        private static UserSessionManager _instance;
        public static UserSessionManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserSessionManager();
                }

                return _instance;
            }
        }


        //private readonly IMvxMessenger _messenger;
        //private readonly IWebApiClientService _webApiClientService;
        //private readonly MvxSubscriptionToken _unauthorizedAccessSubscriptionToken;

        //private ApplicationUser _authenticatedUser;

        public bool IsAuthenticated { get; set; }

        //private string _currentAuthToken;
        public async Task<string> GetCurrentAuthToken()
        {
            return await AuthDataStore.Instance.GetCurrentAuthToken();
        }

        //public AuthenticationService(IWebApiClientService webApiClientService, IMvxMessenger messenger)
        //{
        //_webApiClientService = webApiClientService;

        //_messenger = messenger;
        //_unauthorizedAccessSubscriptionToken = _messenger.Subscribe<UnauthorizedAccessMessage>(OnUnauthorizedAccessMessage);
        //}

        public async Task<LoginResultModel> LoginAsync(string username, string password)
        {
            return await Task.Run(async () =>
             {
                 try
                 {
                     AuthResponseModel result = null;
                     result = await WebApiService.Instance.LoginAsync(username, password);
                     var user = UserModel.FromJwt(result.AccessToken, result.RefreshToken);


                     if (user.HasMobileAccess)
                     {
                         await AuthDataStore.Instance.SetCurrentUser(new UserNameStorageModel { Username = user.UserName, AuthToken = user.AccessToken });
                         return new LoginResultModel(LoginResultStatus.Success);
                     }

                     IsAuthenticated = false;
                     return new LoginResultModel(LoginResultStatus.Fail, "User does not have mobile access permission.");

                 }
                 catch (WebApiException ex)
                 {
                     AuthServerErrorResponse authServerError = null;
                     try
                     {
                         authServerError = JsonConvert.DeserializeObject<AuthServerErrorResponse>(ex.Message);
                     }
                     catch (Exception e)
                     {
                         authServerError = new AuthServerErrorResponse();
                         authServerError.ErrorDescription = e.Message;
                         authServerError.Error = e.Message;
                         //authServerError.Error = ex.StatusCode + " " + ex.Data + " " + ex.Message + " " + ex.InnerException;
                     }

                     if (authServerError != null)
                         return new LoginResultModel(LoginResultStatus.Fail, authServerError.ErrorDescription);

                     return new LoginResultModel(LoginResultStatus.Fail, ex.Message);
                 }
             });
        }

        public async Task LogOffAsync()
        {
            await SetAuthenticatedUserAsync(null);
        }

        //public void ConditionallyShowViewModel(Type authenticatedViewModelType)
        //{
        //    if (!IsAuthenticated)
        //        ShowViewModel<LoginViewModel>();
        //    else
        //        ShowViewModel(authenticatedViewModelType);
        //}

        public async Task SetAuthenticatedUserAsync(UserModel user)
        {
            if (user != null)
            {
                //_authenticatedUser = user;
                IsAuthenticated = true;

                //_messenger.Publish(new UserAuthenticatedMessage(this, user));
            }
            else
            {
                //_messenger.Publish(new UserLoggedOffMessage(this, _authenticatedUser));

                //_authenticatedUser = null;
                IsAuthenticated = false;
            }

            await Task.FromResult(0);
        }


        //public async void UnauthorizedAccess(UserModel user)
        //{
        //    if (message.User != null && !string.IsNullOrEmpty(message.User.RefreshToken))
        //    {
        //        try
        //        {
        //            var result = await _webApiClientService.RefreshAccessTokenAsync(message.User.RefreshToken);
        //            var user = UserModel.FromJwt(result.AccessToken, result.RefreshToken);

        //            if (user.HasMobileAccess)
        //            {
        //                await SetAuthenticatedUserAsync(user);
        //                return;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            bool loggedIn = await TryLogin();
        //            if (loggedIn)
        //                return;
        //        }
        //    }
        //    await LogOffAsync();
        //    //ShowViewModel<LoginViewModel>();
        //}

        //public async void OnUnauthorizedAccessMessage(UnauthorizedAccessMessage message)
        //{
        //    if (message.User != null && !string.IsNullOrEmpty(message.User.RefreshToken))
        //    {
        //        try
        //        {
        //            var result = await _webApiClientService.RefreshAccessTokenAsync(message.User.RefreshToken);
        //            var user = UserModel.FromJwt(result.AccessToken, result.RefreshToken);

        //            if (user.HasMobileAccess)
        //            {
        //                await SetAuthenticatedUserAsync(user);
        //                return;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            bool loggedIn = await TryLogin();
        //            if (loggedIn)
        //                return;
        //        }
        //    }
        //    await LogOffAsync();
        //    //ShowViewModel<LoginViewModel>();
        //}

        //private async Task<bool> TryLogin()
        //{
        //    try
        //    {
        //        //var repo = Mvx.Resolve<IUsernameRepository>();
        //        //Username prevUsername = null;

        //        prevUsername = await repo.GetAsync();
        //        if (prevUsername != null && !string.IsNullOrEmpty(prevUsername.Email) && !string.IsNullOrEmpty(prevUsername.Password))
        //        {
        //            LoginResult loginResult = null;
        //            loginResult = await LoginAsync(prevUsername.Email, prevUsername.Password);
        //            if (loginResult != null && loginResult.Success)
        //                return true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //    }


        //    return false;
        //}
    }


}

