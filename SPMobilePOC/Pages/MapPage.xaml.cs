﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPMobilePOC.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace SPMobilePOC.Pages
{
    public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            InitializeComponent();

            ((MapViewModel)BindingContext).ReloadMapCommand = new Command(() =>
            {


                Map.Pins.Clear();

                foreach (var car in ((MapViewModel)BindingContext).VehicleList)
                {



                    var pin = new Pin
                    {
                        Type = PinType.Generic,
                        Label = car.DriverName ?? "Driver",
                        Address = car.Address,
                        Position = new Position(car.Latitude, car.Longitude),
                        //Rotation = 33.3f,
                        Tag = "id_" + car.DriverName + car.DriverID
                    };

                    Map.Pins.Add(pin);






                }

                //var coordinateBounds = new CoordinateBounds();

                //MutablePath path = new MutablePath();

                //foreach (LighterVehicle vehicle in vehicles)
                //{
                //	if (vehicle != null && vehicle.Latitude != (double)0 && vehicle.Longitude != (double)0)
                //		path.AddLatLon(vehicle.Latitude, vehicle.Longitude); //
                //}

                //coordinateBounds = new CoordinateBounds(path);
                //mapBounds = AdjustBoundsForMaxZoom(coordinateBounds);
                //ZoomToBounds(mapBounds, DpPadding, Animated);

                Device.BeginInvokeOnMainThread(() =>
                {

                    Map.MoveToRegion(MapSpan.FromCenterAndRadius(Map.Pins.First().Position, Distance.FromMeters(50000)), true);

                });



            });

            // MapTypes
            //var mapTypeValues = new List<MapType>();
            //foreach (var mapType in Enum.GetValues(typeof(MapType)))
            //{
            //    mapTypeValues.Add((MapType)mapType);
            //    pickerMapType.Items.Add(Enum.GetName(typeof(MapType), mapType));
            //}

            //pickerMapType.SelectedIndexChanged += (sender, e) =>
            //{
            //    map.MapType = mapTypeValues[pickerMapType.SelectedIndex];
            //};
            //pickerMapType.SelectedIndex = 0;

            // MyLocationEnabled
            //switchMyLocationEnabled.Toggled += (sender, e) =>
            //{
            //    map.MyLocationEnabled = e.Value;
            //};
            //switchMyLocationEnabled.IsToggled = map.MyLocationEnabled;

            //// IsTrafficEnabled
            //switchIsTrafficEnabled.Toggled += (sender, e) =>
            //{
            //    map.IsTrafficEnabled = e.Value;
            //};
            //switchIsTrafficEnabled.IsToggled = map.IsTrafficEnabled;

            //// IndoorEnabled
            //switchIndoorEnabled.Toggled += (sender, e) =>
            //{
            //    map.IsIndoorEnabled = e.Value;
            //};
            //switchIndoorEnabled.IsToggled = map.IsIndoorEnabled;

            //// CompassEnabled
            //switchCompassEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.CompassEnabled = e.Value;
            //};
            //switchCompassEnabled.IsToggled = map.UiSettings.CompassEnabled;

            //// RotateGesturesEnabled
            //switchRotateGesturesEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.RotateGesturesEnabled = e.Value;
            //};
            //switchRotateGesturesEnabled.IsToggled = map.UiSettings.RotateGesturesEnabled;

            //// MyLocationButtonEnabled
            //switchMyLocationButtonEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.MyLocationButtonEnabled = e.Value;
            //};
            //switchMyLocationButtonEnabled.IsToggled = map.UiSettings.MyLocationButtonEnabled;

            //// IndoorLevelPickerEnabled
            //switchIndoorLevelPickerEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.IndoorLevelPickerEnabled = e.Value;
            //};
            //switchIndoorLevelPickerEnabled.IsToggled = map.UiSettings.IndoorLevelPickerEnabled;

            //// ScrollGesturesEnabled
            //switchScrollGesturesEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.ScrollGesturesEnabled = e.Value;
            //};
            //switchScrollGesturesEnabled.IsToggled = map.UiSettings.ScrollGesturesEnabled;

            //// TiltGesturesEnabled
            //switchTiltGesturesEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.TiltGesturesEnabled = e.Value;
            //};
            //switchTiltGesturesEnabled.IsToggled = map.UiSettings.TiltGesturesEnabled;

            //// ZoomControlsEnabled
            //switchZoomControlsEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.ZoomControlsEnabled = e.Value;
            //};
            //switchZoomControlsEnabled.IsToggled = map.UiSettings.ZoomControlsEnabled;

            //// ZoomGesturesEnabled
            //switchZoomGesturesEnabled.Toggled += (sender, e) =>
            //{
            //    map.UiSettings.ZoomGesturesEnabled = e.Value;
            //};
            //switchZoomGesturesEnabled.IsToggled = map.UiSettings.ZoomGesturesEnabled;

            //// Map Clicked
            //map.MapClicked += (sender, e) =>
            //{
            //    var lat = e.Point.Latitude.ToString("0.000");
            //    var lng = e.Point.Longitude.ToString("0.000");
            //    this.DisplayAlert("MapClicked", $"{lat}/{lng}", "CLOSE");
            //};

            //// Map Long clicked
            //map.MapLongClicked += (sender, e) =>
            //{
            //    var lat = e.Point.Latitude.ToString("0.000");
            //    var lng = e.Point.Longitude.ToString("0.000");
            //    this.DisplayAlert("MapLongClicked", $"{lat}/{lng}", "CLOSE");
            //};

            //// Map MyLocationButton clicked
            //map.MyLocationButtonClicked += (sender, args) =>
            //{
            //    args.Handled = switchHandleMyLocationButton.IsToggled;
            //    if (switchHandleMyLocationButton.IsToggled)
            //    {
            //        this.DisplayAlert("MyLocationButtonClicked",
            //                     "If set MyLocationButtonClickedEventArgs.Handled = true then skip obtain current location",
            //                     "OK");
            //    }

            //};

            //map.CameraChanged += (sender, args) =>
            //{
            //    var p = args.Position;
            //    labelStatus.Text = $"Lat={p.Target.Latitude:0.00}, Long={p.Target.Longitude:0.00}, Zoom={p.Zoom:0.00}, Bearing={p.Bearing:0.00}, Tilt={p.Tilt:0.00}";
            //};

            // Geocode
            //buttonGeocode.Clicked += async (sender, e) =>
            //{
            //    var geocoder = new Xamarin.Forms.GoogleMaps.Geocoder();
            //    var positions = await geocoder.GetPositionsForAddressAsync(entryAddress.Text);
            //    if (positions.Count() > 0)
            //    {
            //        var pos = positions.First();
            //        map.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMeters(5000)));
            //        var reg = map.VisibleRegion;
            //        var format = "0.00";
            //        labelStatus.Text = $"Center = {reg.Center.Latitude.ToString(format)}, {reg.Center.Longitude.ToString(format)}";
            //    }
            //    else
            //    {
            //        await this.DisplayAlert("Not found", "Geocoder returns no results", "Close");
            //    }
            //};

            // Snapshot
            //buttonTakeSnapshot.Clicked += async (sender, e) =>
            //{
            //    var stream = await map.TakeSnapshot();
            //    imageSnapshot.Source = ImageSource.FromStream(() => stream);
            //};





            //Pin pinTokyo = null;
            //Pin pinNewYork = null;

            //// Tokyo pin
            ////buttonAddPinTokyo.Clicked += (sender, e) =>
            ////{
            //pinTokyo = new Pin()
            //{
            //    Type = PinType.Place,
            //    Label = "Tokyo SKYTREE",
            //    Address = "Sumida-ku, Tokyo, Japan",
            //    Position = new Position(35.71d, 139.81d),
            //    Rotation = 33.3f,
            //    Tag = "id_tokyo",
            //    //IsVisible = switchIsVisibleTokyo.IsToggled
            //};

            //map.Pins.Add(pinTokyo);
            //map.MoveToRegion(MapSpan.FromCenterAndRadius(pinTokyo.Position, Distance.FromMeters(5000)));

            //((Button)sender).IsEnabled = false;
            //buttonRemovePinTokyo.IsEnabled = true;
            //};

            //buttonRemovePinTokyo.Clicked += (sender, e) =>
            //{
            //    map.Pins.Remove(pinTokyo);
            //    pinTokyo = null;
            //    ((Button)sender).IsEnabled = false;
            //    buttonAddPinTokyo.IsEnabled = true;
            //};
            //buttonRemovePinTokyo.IsEnabled = false;

            // New York pin
            //buttonAddPinNewYork.Clicked += (sender, e) =>
            //{
            //pinNewYork = new Pin()
            //{
            //    Type = PinType.Place,
            //    Label = "Central Park NYC",
            //    Address = "New York City, NY 10022",
            //    Position = new Position(40.78d, -73.96d),
            //    Tag = "id_new_york"
            //};

            //map.Pins.Add(pinNewYork);
            //map.MoveToRegion(MapSpan.FromCenterAndRadius(pinNewYork.Position, Distance.FromMeters(5000)));

            //    ((Button)sender).IsEnabled = false;
            //    buttonRemovePinNewYork.IsEnabled = true;
            //};

            //buttonRemovePinNewYork.Clicked += (sender, e) =>
            //{
            //    map.Pins.Remove(pinNewYork);
            //    pinNewYork = null;
            //    ((Button)sender).IsEnabled = false;
            //    buttonAddPinNewYork.IsEnabled = true;
            //};
            //buttonRemovePinNewYork.IsEnabled = false;

            // Clear Pins
            //buttonClearPins.Clicked += (sender, e) =>
            //{
            //map.Pins.Clear();

            //pinTokyo = null;
            //pinNewYork = null;
            //buttonAddPinTokyo.IsEnabled = true;
            //buttonAddPinNewYork.IsEnabled = true;
            //buttonRemovePinTokyo.IsEnabled = false;
            //buttonRemovePinNewYork.IsEnabled = false;
            //};

            // Select New York Pin
            //buttonSelectPinNewYork.Clicked += (sender, e) =>
            //{
            //    if (pinNewYork == null)
            //    {
            //        DisplayAlert("Error", "New York is not added.", "Close");
            //        return;
            //    }

            //    map.SelectedPin = pinNewYork;
            //};

            // Clear Pin Selection
            //buttonClearSelection.Clicked += (sender, e) =>
            //{
            //    if (map.SelectedPin == null)
            //    {
            //        DisplayAlert("Error", "Pin is not selected.", "Close");
            //        return;
            //    }

            //    map.SelectedPin = null;
            //};


            // Visible/Invisible Pin tokyo
            //switchIsVisibleTokyo.Toggled += (sender, args) =>
            //{
            //    if (pinTokyo != null)
            //    {
            //         pinTokyo.IsVisible = args.Value;
            //    }
            //};

            //// AnchorX Pin new york
            //sliderAnchorXNewyork.ValueChanged += (sender, args) =>
            //{
            //    pinNewYork.Anchor = new Point(args.NewValue / 100d, pinNewYork.Anchor.Y);
            //};

            //// AnchorY Pin new york
            //sliderAnchorYNewyork.ValueChanged += (sender, args) =>
            //{
            //    pinNewYork.Anchor = new Point(pinNewYork.Anchor.X, args.NewValue / 100d);
            //};

            Map.PinClicked += Map_PinClicked;

            // Selected Pin changed
            Map.SelectedPinChanged += SelectedPin_Changed;

            Map.InfoWindowClicked += InfoWindow_Clicked;

            Map.InfoWindowLongClicked += InfoWindow_LongClicked;
        }

        private void InfoWindow_LongClicked(object sender, InfoWindowLongClickedEventArgs e)
        {

            //var time = DateTime.Now.ToString("hh:mm:ss");
            //labelStatus.Text = $"[{time}]InfoWindow Long Clicked - {e?.Pin?.Tag.ToString() ?? "nothing"}";
        }

        private void InfoWindow_Clicked(object sender, InfoWindowClickedEventArgs e)
        {
            //var time = DateTime.Now.ToString("hh:mm:ss");
            //labelStatus.Text = $"[{time}]InfoWindow Clicked - {e?.Pin?.Tag.ToString() ?? "nothing"}";
        }

        void SelectedPin_Changed(object sender, SelectedPinChangedEventArgs e)
        {
            //var time = DateTime.Now.ToString("hh:mm:ss");
            //labelStatus.Text = $"[{time}]SelectedPin changed - {e?.SelectedPin?.Label ?? "nothing"}";
        }

        // Do NOT mark async method.
        // Because Xamarin.Forms.GoogleMaps wait synchronously for this callback returns.
        void Map_PinClicked(object sender, PinClickedEventArgs e)
        {
            //e.Handled = switchHandlePinClicked.IsToggled;

            // If you set e.Handled = true,
            // then Pin selection doesn't work automatically.
            // All pin selection operations are delegated to you.
            // Sample codes are below.
            //if (switchHandlePinClicked.IsToggled)
            //{
            //    map.SelectedPin = e.Pin;
            //    map.AnimateCamera(CameraUpdateFactory.NewPosition(e.Pin.Position));
            //}
        }
    }
}
