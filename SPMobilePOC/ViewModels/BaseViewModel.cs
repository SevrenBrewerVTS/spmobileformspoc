﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace SPMobilePOC.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public abstract class BaseViewModel
    {
        protected BaseViewModel()
        {
            Guid = Guid.NewGuid();
        }

        public class BaseViewModelProperyChangedEventArgs : EventArgs
        {
            public bool IsBusyIndicatorVisible { get; private set; }
            public string BusyText { get; private set; }
            public BaseViewModelProperyChangedEventArgs(bool isBusyIndicatorVisible, string busyText)
            {
                IsBusyIndicatorVisible = isBusyIndicatorVisible;
                BusyText = busyText;
            }
        }

        public event EventHandler<BaseViewModelProperyChangedEventArgs> IsBusyIndicatorVisibleProperyChanged;
        public event EventHandler<BaseViewModelProperyChangedEventArgs> BusyTextProperyChanged;

        public abstract Task Start();
        public abstract Task Stop();
        public abstract Task Refresh();

        string BusyText { get; set; }
        bool IsBusyIndicatorVisible { get; set; }
        public Guid Guid { get; set; }
        public INavigation Navigation { get; set; }

        #region Busy View

        public void OnIsBusyIndicatorVisibleChanged()
        {
            IsBusyIndicatorVisibleProperyChanged?.Invoke(this, new BaseViewModelProperyChangedEventArgs(IsBusyIndicatorVisible, BusyText));
        }

        public void OnBusyTextChanged()
        {
            BusyTextProperyChanged?.Invoke(this, new BaseViewModelProperyChangedEventArgs(IsBusyIndicatorVisible, BusyText));
        }

        public void ShowBusyIndicator(string message = "Loading...")
        {
            BusyText = message;
            IsBusyIndicatorVisible = true;
        }

        public void HideBusyIndicator()
        {
            IsBusyIndicatorVisible = false;
        }

        #endregion
        #region Navigation

        public void NavigateToPagePush(Page page, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushAsync(page, animated);
                completion?.Invoke();
            });
        }

        public void NavigateToPagePush(Type pageType, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var page = (Page)Activator.CreateInstance(pageType);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushAsync(page);
                completion?.Invoke();
            });
        }

        public void NavigateToPageModal(Page page, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var navView = new NavigationPage(page);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushModalAsync(navView, animated);
                completion?.Invoke();
            });
        }

        public void NavigateToPageModal(Type pageType, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var navView = new NavigationPage((Page)Activator.CreateInstance(pageType));
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushModalAsync(navView);
                completion.Invoke();
            });
        }

        public void PopAsync(bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PopAsync(animated);
                completion?.Invoke();
            });
        }

        //public void PushPopupAsync(PopupPage view, Action completion = null)
        //{
        //    if (Navigation == null)
        //    {
        //        throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
        //    }

        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        await Navigation.PushPopupAsync(view);
        //        completion?.Invoke();
        //    });
        //}

        //public void PopPopup()
        //{
        //    if (Navigation == null)
        //    {
        //        throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
        //    }

        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        try
        //        {
        //            await Navigation.PopPopupAsync();
        //        }
        //        catch { }
        //    });
        //}

        //public async Task PopPopupAsync()
        //{
        //    try
        //    {
        //        if (Navigation == null)
        //        {
        //            throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
        //        }

        //        await Navigation.PopPopupAsync();
        //    }
        //    catch { }

        //    //await Device.BeginInvokeOnMainThread(async () =>
        //    //{
        //    //    try
        //    //    {
        //    //        await Navigation.PopPopupAsync();
        //    //    }
        //    //    catch { }
        //    //});
        //}

        public void PopToRoot(bool animated = true, Action completion = null)
        {

            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PopToRootAsync(animated);
                completion?.Invoke();
            });
        }

        #endregion
        #region Alert View

        public void DisplayAlert(string title, string message, string buttonText, Action completion = null)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Application.Current.MainPage.DisplayAlert(title, message, buttonText);
                completion?.Invoke();
            });
        }

        public void DisplayAlert(string title, string message, string acceptButtonText, string cancleButtonText, Action acceptCompletion = null)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (await Application.Current.MainPage.DisplayAlert(title, message, acceptButtonText, cancleButtonText))
                {
                    acceptCompletion?.Invoke();
                }
            });
        }

        #endregion

        // NOTE: We use FodyWeavers.PropertyChanged to simplify the bindable
        // properties

        // i.e. public string Email { get; set; }

        // property changes can be observed like so

        // public void OnPropertyChanged(string propertyName, object before, object after) {}

        // OR

        // public void On<NameOfPropertyHere>Changed() {}

        // to enable add [AddINotifyPropertyChangedInterface] above the View Model's Class name

    }
}
