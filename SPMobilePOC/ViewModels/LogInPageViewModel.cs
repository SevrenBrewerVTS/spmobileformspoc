﻿
using System.Threading.Tasks;
using PropertyChanged;
using SPMobilePOC.Pages;
using WebService;
using Xamarin.Forms;

namespace SPMobilePOC.ViewModels
{
    public class LogInPageViewModel : BaseViewModel
    {
        public LogInPageViewModel()
        {
            Email = "sevren.brewer@vehicletracking.com";
            Password = "Hamm3rBr0th3rs";
        }

        public string Email { get; set; }
        public string Password { get; set; }

        public Command ForgotPasswordCommand => new Command(async () =>
        {

        });

        public Command LoginCommand => new Command(async () =>
        {

            //await Application.Current.MainPage.Navigation.PushModalAsync(new MapPage());
            //return;

            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Email or Username is required.", "OK");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Password is required.", "OK");
                return;
            }

            var result = await UserSessionManager.Instance.LoginAsync(Email, Password);

            if (result.Status == WebService.Models.LoginResultStatus.Success)
            {
                await Application.Current.MainPage.Navigation.PushModalAsync(new MapPage());
                return;
            }


            await Application.Current.MainPage.DisplayAlert("Error", "Error logging in. Please try again.", "OK");

        });

        public async override Task Refresh()
        {

        }

        public async override Task Start()
        {

        }

        public async override Task Stop()
        {

        }
    }
}
