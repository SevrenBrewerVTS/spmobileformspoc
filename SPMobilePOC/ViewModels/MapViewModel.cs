﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPMobilePOC.WebServices.Models;
using WebService;
using System.Linq;
using Xamarin.Forms;

namespace SPMobilePOC.ViewModels
{
    public class MapViewModel : BaseViewModel
    {

        public List<VehicleDTO> VehicleList { get; private set; }

        public Command ReloadMapCommand { get; set; }

        public MapViewModel()
        {
            Task.Run(async () =>
            {
                var authToken = await UserSessionManager.Instance.GetCurrentAuthToken();

                var vehicles = await WebApiService.Instance.GetVehiclesAsync(authToken);

                VehicleList = vehicles.ToList();


                ReloadMapCommand?.Execute(null);
            });


        }

        public async override Task Refresh()
        {


        }

        public async override Task Start()
        {


        }

        public async override Task Stop()
        {
        }
    }
}
